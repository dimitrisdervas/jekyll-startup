---
title: Page.html
---


## Create a Section
Choose the htmlelement or use custom html code from a file
  - div, section, article, ...
  - custom
  - hr

and check how we want to
  - theme
  - wrap
  - add title and subtitle

## Create a Div inside a section
  Create a div and call a component or add custom html on the spot

## Use a Component
  title or subtitle
  wrap the component or not

### YAML BAREBONE
      config:
       - section:
          data:
            1. htmlelement: section
            2. custom: blocks/hero.html
            3. hr:
          seoTitle:
            data:
              label:
              separator:
              link:
          seoSubtitle:
            data:
              label:
              separator:
              link:
          wrapperSection:
            theme:
              class:
          theme:
            class:
          divs:
            - div:
               theme:
                 class:
              components:
              1. html: '<i class="material-icons">menu</i>'
              2. component:



### YAML Settings with Comments
    config:
     - section:
        data: # one of the following
          1. htmlelement: section # the element that we want to assign ex.article, div etc.
          2. custom: blocks/hero.html # path to custom file inside _includes folder ex. blocks/hero.html
          3. hr: #you can leave it empty if you want to print a <hr>
        seoTitle: # Check if there is a title for the html section and print it [ OPTIONAL ]
          data:
            label: # the label to use for the title
            separator: # use a separator ex.: / or -
            link: # make the title a link
        seoSubtitle: # Check if there is a subtitle for the html section and print it [ OPTIONAL ]
          data:
            label: # the label to use for the title
            separator: # use a separator ex.: / or -
            link: # make the title a link
        wrapperSection: # Check if want to wrap the section in a div [ OPTIONAL ]
          theme: # Theme the wrapperSection - after theme you can add any attributes that you want that you can inside a div ex.[ <div class="test" id="test1" data-id="2"]
            class: test
            id: test1
            data-id: test2
        theme: # [ REQUIRED ] Theme the Section after theme you can add any attributes that you want that you can inside a div ex <div class="test" id="test1" data-id="2"
          class: test
          id: test1
          data-id: test2
        divs: # Assign divs inside the section one or more [ REQUIRED ]
          - div: # create div [ OPTIONAL ]
             theme: # Theme the div
               class: test
               id: test1
               data-id: test2
            components: # Use pre-made components inside the div [ REQUIRED ]
            1. html: <i class="material-icons">menu</i> # You can write html code also here if you do not want a component
            2. component: one-post # use a premade component or use a custom one inside the _includes/html_components folder
