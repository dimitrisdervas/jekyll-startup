---
title: list posts
categories: list-posts main
---

### YAML BAREBONES
      - component: posts-list
        data:
          data-type:
          1. data-list
          2. collection
          content-type:
           1. page.news
           2. <name of the file or collection>
          tag:
          tag-hash:
          folder:
          post-title:
          groupElement:
            theme:
              id:
              class:
          groupdiv:
            theme:
              id:
              class:
            fields:
          groupFieldsElement:
            1. div
            2. ul
            3. no
            theme:
              id:
              class:
           limit:
           offset:
          fields:

### YAML WITH COMMENTS
    - component: posts-list # Posts list [ REQUIRED ]
      data:
        data-type: [ REQUIRED ]
        1. data-list
        2. collection
        content-type: # DEBUG FILE: html_components/component_includes/componentsContentType.html [ REQUIRED ]
         1. page.news # the file resides in _ data folder and use a variable from the Front Matter ex. _data/1012.yml
         2. <name of the file or collection>
        tag: # IF we want to look for a specific title-tag or whatever you call it inside a .yml file ex. page.user
        tag-hash: # The hash inside which ex. the users reside
        folder: # if the file resides in a folder inside the _data folder ex. _data/success/1012.yml
        post-title: # If we want to find a certain post ex."The Good news" it matches the title to the post title
        groupElement: # the element to use to group the component [ OPTIONAL ]
          theme:
            id:
            class:
        groupdiv: # the element to use to group all the posts [ OPTIONAL ]
          theme:
            id:
            class:
          fields: # the rest of the fields
        groupFieldsElement: # the element to use to group each of the posts [ OPTIONAL ]
          1. div
          2. ul # it is the default
          3. no # no element to group each posts
          theme:
            id:
            class:
         limit: # the number of posts to show [ OPTIONAL ]
         offset: # the number of posts to offset [ OPTIONAL ]
        fields: # the fields to print [ REQUIRED ]
