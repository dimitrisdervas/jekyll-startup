---
title: COLLECTION - LIST POSTS
tag: list-posts
description: |
          loop through the posts of collection
parameters:
    data-type: |
      data-list if you have a file .csv or .yml in _data
      collection if you have posts in a collection
    tag: |
      IF we want to look for a specific title-tag or whatever you call it inside a .yml file ex. page.user
    tag-hash: The hash inside which ex. the users reside
    folder: if the file resides in a folder inside the _data folder ex. _data/success/1012.yml
    content-type: |
        page.news the file resides in _data folder and use a variable from the Front Matter ex. _data/1012.yml
        name of the file or collection
    groupElement: | 
        the element to use to group the component plus theme 
          theme: 
            id: 
            class:
    groupdiv: |
         the element to use to group all the posts 
          theme: 
            id: 
            class:
    groupFieldsElement: |
      the element to use to group each of the posts
      div
      ul - it is the default
      no - no element to group each posts theme: id: class:
    limit: the number of posts to show
    offset: the number of posts to offset
    fields: the fields to print
yaml: |
    - component: posts-list
       data:
         data-type:
         tag:
         tag-hash:
         folder:
         content-type: 
         groupElement:
           theme:
             id:
             class:
         groupdiv:
           theme:
             id:
             class:
         groupFieldsElement:
           theme:
             id:
             class:
          limit:
          offset:
         fields:
debug: html_components/posts-list.html
example: 
demo-config: |
  - section:
      data:
        htmlelement: section
    divs:
       - div:
           theme:
            class: classDiv
         components:
          - component: posts-list
            data:
              data-type: collection
              content-type: test-collection
              fields:
                - field: title
                - field: text
                  data:
                     text-teaser: yes
                     text-words: 15
                     more-label: more
                     class: text:
config:
  - section:
      data:
        htmlelement: section
    divs:
       - div:
           theme:
            class: classDiv
         components:
          - component: posts-list
            data:
              data-type: collection
              content-type: test-collection
              fields:
                - field: title
---

{% include basic_components/page.html %}
