---
title: TEXT FIELD
tag: fieldcase
description: |
 Show the text of a post of a filed in data file and **strips html**
parameters:
    text-teaser: somevalue true or yes or anything
    text-words: "if not it will use the excerpt separator <!--more-->"
    more-label: if show more link and link to post.url
    class: theme
yaml: |
    - field: text
        data:
          text-teaser:
          text-words:
          more-label:
          class:
debug: html_components/fieldscases/titlecase.html
example: |
    <!-- DEBUG FILE: html_components/fieldscases/textCase.html -->
    <!-- If text is about blog which means the to print the post.content -->
    {% if include.data-type == 'collection' %}
          <!-- If it is about a blog teaser -->
          {% if include.field_Data.text-teaser != nill %}
            <!-- Check if there is words limit or use the excerpt separator -->
            {% if include.field_Data.text-words %}
              <!-- The syntax it is not right but it works - I ommited }} at the end -->
              <div class="{{include.field_Data.class}}">{{ post.content | truncatewords: include.field_Data.text-words | strip_html }}
              <div class="more {{include.field_Data.more-class}}"><a href="{{post.url}}">{{include.field_Data.more-label}}</a></div></div>
            {% else %}
                <div class="{{include.field_Data.class}}">{{ post.content | split:'<!--more-->' | first | strip_html }}
                <div class="more {{include.field_Data.more-class}}"><a href="{{post.url}}">{{include.field_Data.more-label}}</a></div>
                </div>
            {% endif %}
          {% else %}
             <div class="{{include.field_Data.class}}">{{ post.content }}</div>
          {% endif %}
    {% else %}
          <!-- If it is about a simple text field -->
          <!-- and there is a teaser -->
          <div class="{{include.field_Data.class}}">
          {% if include.field_Data.text-teaser != nill %}
            <!-- Check if there is words limit or use the excerpt separator -->
            {% if include.field_Data.text-words %}
                {{ post.text | truncatewords: include.field_Data.text-words }}
            {% else %}
                {{ post.text | split:'<!--more-->' | first }}
            {% endif %}
          {% else %}
              {{ post.text }}
          {% endif %}
           </div>
    {% endif %}
demo-config: |
  - section:
      data:
        htmlelement: section
    divs:
       - div:
           theme:
            class: classDiv
         components:
          - component: one-post
            data:
              data-type: collection
              content-type: test-collection
              post-title: 'one post'
              fields:
                - field: title
                - field: text
                  data:
                     text-teaser: yes
                     text-words: 150
                     more-label: more
                     class: text
config:
  - section:
      data:
        htmlelement: section
    divs:
       - div:
           theme:
            class: classDiv
         components:
          - component: one-post
            data:
              data-type: collection
              content-type: test-collection
              post-title: 'one post'
              fields:
                - field: title
                - field: text
                  data:
                     text-teaser: yes
                     text-words: 150
                     more-label: more
                     class: text
---
