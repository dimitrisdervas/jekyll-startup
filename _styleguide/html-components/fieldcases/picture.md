---
title: PICTURE FIELD
tag: fieldcase
description: |
  The button component should be used as the call-to-action in a form, or as a
  user interaction mechanism. Generally speaking, a button should not be used
  when a link would do the trick.
parameters:
  content: "*(mandatory)* the content of the button"
  type: "*(optional)* either `button` or `submit` for the `type` HTML attribute
        (default to `button`)"
  class: "*(optional)* any extra class"
yaml: |
    - field: srcset
      data:
         theme:
           class:
         link:
         srcset: large.jpg 1024w, medium.jpg 640w, small.jpg 320w
         sizes: (min-width: 36em) 33.3vw, 100v
         image-hash: image
         image-path:
debug: html_components/fieldscases/picturecase.html
yaml-comments: |
    - field: srcset
      data:
         theme:
           class:
         link:
         srcset: large.jpg 1024w, medium.jpg 640w, small.jpg 320w
         sizes: (min-width: 36em) 33.3vw, 100v
         image-hash: image
         image-path:
example: |
    <!-- DEBUG FILE: html_components/fieldscases/imageCase.html -->
    <!-- Loop through the images and set limit -->
    <div {% include basic_components/pageincludes/themeelement.html elementData=include.field_Data.theme %} >
      <picture>
          {% case include.field_Data.link %}
             {% when 'post' %}
                   <a href="{{ site.baseurl}}{{ post.url }}"> {% capture linkEND %} </a> {% endcapture %}
             {% when 'products' %}
                   <a href="{{ site.baseurl}}products/{{ post.sku }}.html"> {% capture linkEND %} </a> {% endcapture %}
             {% endcase %}

          {% if post.[include.field_Data.image-hash] contains 'https://' or post.[include.field_Data.image-hash] contains 'http://' %}
                <source media="(min-width: 40em)" srcset="big.jpg 1x, big-hd.jpg 2x">
                <source srcset="small.jpg 1x, small-hd.jpg 2x">
                <img src="fallback.jpg" alt="">
                  <img
                      src="{{ post.[include.field_Data.image-hash] }}"
                      alt="{{ post.alt }}">
               {% else %}
                  {% for source in post.picture %}
                    <source {% for data in source.source %}{{data[0]}}="{{data[1]}}"{% endfor %}>
                  {% endfor %}
                  <img
                      src="{{ site.baseurl}}{{ include.field_Data.image-path }}{{ post.img }}"
                      alt="{{ post.alt }}">
               {% endif %}
         {{ linkEND }}
       </picture>
     </div>
---

{% include basic_components/page.html %}