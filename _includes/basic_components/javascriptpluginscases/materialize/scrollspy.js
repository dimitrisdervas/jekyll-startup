$('.scrollspy').scrollSpy();
var length = $('.sidebar-suggestions').height() - $('.scrollspy-block').height() + $('.sidebar-suggestions').offset().top;

    $(window).scroll(function () {

        var scroll = $(this).scrollTop();
        var height = $('.scrollspy-block').height() + 'px';

        if (scroll < $('.sidebar-suggestions').offset().top) {

            $('.scrollspy-block').css({
                'position': 'relative',
                'top': '75px'
            });

        } else if (scroll > length) {

            $('.scrollspy-block').css({
                'position': 'fixed',
                'top': '75px'
            });

        } else {

            $('.scrollspy-block').css({
                'position': 'relative',
                'top': '75px'
            });

        }
    });
