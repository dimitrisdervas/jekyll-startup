var gulp           = require('gulp');
var browserSync    = require('browser-sync');
var sass           = require('gulp-sass');
var prefix         = require('gulp-autoprefixer');
var cp             = require('child_process');
var sourcemaps     = require('gulp-sourcemaps');
var concat         = require('gulp-concat');
var nano           = require('gulp-cssnano');
var util           = require('gulp-util');
var gulpif         = require('gulp-if');
var plumber        = require('gulp-plumber');
var notify         = require('gulp-notify');
var uglify         = require('gulp-uglify');
var imagemin       = require('gulp-imagemin');
var pngquant       = require('imagemin-pngquant');
var fs             = require('fs');
var handlebars     = require('gulp-compile-handlebars');
var rename         = require('gulp-rename');
var baby           = require('babyparse');
var mainBowerFiles = require('gulp-main-bower-files');
var psi            = require('psi');
var htmlmin        = require('gulp-htmlmin');
var gulpFilter     = require('gulp-filter');
var critical       = require('critical');
var googleWebFonts = require('gulp-google-webfonts');
var stripComments  = require('gulp-strip-comments');
var icons          = require("gulp-material-icons");
var shell          = require("gulp-shell");
var postcss        = require('gulp-postcss');
var autoprefixer   = require('autoprefixer');
var pxtorem        = require('postcss-pxtorem');
var orderedValues  = require('postcss-ordered-values');
var colorHexAlpha  = require("postcss-color-hex-alpha");
var responsiveType = require("postcss-responsive-type");
var debug          = require('postcss-debug').createDebugger();

//
// ACHILEAS
//
var messages = {
    jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'
};
/**
 * Store paths
 */
var config = {
    assetsDir: '_resources',
    bowerDir: '_resources/bower/bower.json',
    sassPattern: 'sass/**/*.scss',
    jsPattern: 'js/**/*.js',
    production: !!util.env.production,
    sourceMaps: !util.env.production };
/**
 * Utils object
 */
var app = {};

/**
 * Build the Jekyll Site
 * https://gist.github.com/azmenak/e699fc772e6dcd77e375
 */
// gulp.task("jekyll-build", shell.task([
//   "cd " + './' + " && bundle exec jekyll build --incremental "
// ]));

/**
 * Build the Jekyll Site
 */
gulp.task('jekyll-build', function (done) {
    'use strict';
    browserSync.notify(messages.jekyllBuild);
    return cp.spawn('jekyll', ['build', '--incremental'], {stdio: 'inherit'})
        .on('close', done);
});

/**
 * Rebuild Jekyll & do page reload
 */
gulp.task('jekyll-rebuild', ['jekyll-build','removeHtmlComments'], function () {
    'use strict';
   browserSync.reload();
});


/**
 * Wait for jekyll-build, then launch the Server
 */
gulp.task('browser-sync', ['styles','jekyll-build'], function() {
    browserSync({
        server: {
            baseDir: '_site'
        }
    });
});

/**
 * Compile files from _resources/_scss into both [project-folder]/_site/css (for live injecting) and [project-folder]/css (for future jekyll builds)
 */
gulp.task('styles', function () {
  var processors = [
    autoprefixer({browsers: ['last 1 version']}),
    pxtorem({replace: false,selectorBlackList: ['btn-floating']}),
    colorHexAlpha(),
    responsiveType(),
  ];
   return gulp.src(config.assetsDir+'/sass/styles.scss')
        .pipe(plumber({ errorHandler: function(err) {
            notify.onError({
                title: "Gulp error in " + err.plugin,
                message:  err.toString()
            })(err);
        }}))
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(sass({
            includePaths: ['scss']
        }))
        .pipe(postcss(debug(processors)))
        .pipe(concat('styles.css'))
        .pipe(config.production ? nano() : util.noop())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('_site/assets/css'))
        .pipe(browserSync.reload({stream:true}))
        .pipe(gulp.dest('assets/css'));

});

gulp.task('css-debug', ['styles'], function () {
  debug.inspect()
})


/*
* Critical CSS
*/
gulp.task('critical', function () {
    critical.generate({
        base: './',
        src: '_site/index.html',
        css: '_site/assets/css/styles.css',
        dest: '_includes/css/critical.css',
        width: 320,
        height: 480,
        minify: true
    });
});


/**
 * Compile fonts
 */
gulp.task('fonts', function () {

    var options = {
    fontsDir: 'googlefonts/',
    cssDir: 'googlecss/',
    cssFilename: 'myGoogleFonts.css'
    };

    return gulp.src('_resources/fonts/fonts.list')
    .pipe(googleWebFonts(options))
    .pipe(gulp.dest('_resources/fonts'))
    ;
});


gulp.task('images', function () {
  // Select all images in assets/images and resize
  return gulp.src(config.assetsDir+'/_uploaded/**/*.{jpg,png}')
      .pipe(responsive({
        // Resize all JPG images to three different sizes: 200, 500, and 630 pixels
        '**/*.jpg': [{
          width: 200,
          rename: { suffix: '-200px' },
        }, {
          width: 500,
          rename: { suffix: '-500px' },
        }, {
          width: 630,
          rename: { suffix: '-630px' },
        }, {
          // Compress, strip metadata, and rename original image
          rename: { suffix: '-original' },
        }],
        // Resize all PNG images to be retina ready
        '**/*.png': [{
          width: 250,
        }, {
          width: 250 * 2,
          rename: { suffix: '@2x' },
        }],
      }, {
        errorOnUnusedImage: true,
        errorOnEnlargement: false,
        // Global configuration for all images
        // The output quality for JPEG, WebP and TIFF output formats
        quality: 70,
        // Use progressive (interlace) scan for JPEG and PNG output
        progressive: true,
        // Strip all metadata
        withMetadata: false,
      }))
    .pipe(gulp.dest('_site/assets/images/'))
    .pipe(gulp.dest('assets/images/'));
});


/**
 * Watch scss files for changes & recompile
 * Watch html/md files, run jekyll & reload BrowserSync
 * ADD ALL THE COLLECTIONS folders
 */
gulp.task('watch', function () {
    gulp.watch(config.assetsDir+'/sass/**/*.scss', ['styles']);
    gulp.watch(config.assetsDir+'/js/**/*.js', ['scripts']);
});

// WATCH ONLY SASS STYLES
gulp.task('watch-only-styles', function () {
  gulp.watch(config.assetsDir+'/sass/*.scss', ['styles']);
});

gulp.task('watch-styles', ['browser-sync', 'watch-only-styles']);


/**
 * Util functions
 */
app.addStyle = function(paths, outputFilename){
    gulp.src(paths)
        .pipe(plumber())
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(sass({
            includePaths: ['scss'],
            onError: browserSync.notify
        }))
        .pipe(postcss([ require('autoprefixer') ],[assets({
      loadPaths: ['images/']
    })],[ sorting({ "empty-lines-between-children-rules": 1 , "sort-order": "zen" }) ]))
        .pipe(concat(outputFilename))
        .pipe(config.production ? nano() : util.noop())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('_site/assets/css'))
        .pipe(browserSync.reload({stream:true}))
        .pipe(gulp.dest('assets/css'));
};

app.addScripts = function(paths, outputFilename){
    gulp.src(paths)
        .pipe(plumber())
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(concat(outputFilename))
        .pipe(config.production ? uglify() : util.noop())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('_site/js'))
        .pipe(browserSync.reload({stream:true}))
        .pipe(gulp.dest('js'));
};

app.copy = function(srcFiles, outputDir) {
    gulp.src(srcFiles)
        .pipe(gulp.dest(outputDir))
    ;
};

gulp.task('main-bower-files-js', function(){
    var filterJS = gulpFilter('**/*.js', { restore: true });
    return gulp.src('./_resources/bower/bower.json')
        .pipe(mainBowerFiles( ))
        .pipe(filterJS)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('assets/js'));
});

gulp.task('main-bower-files-css', function(){
    var filterJS = gulpFilter('**/*.css', { restore: true });
    return gulp.src('./_resources/bower/bower.json')
        .pipe(mainBowerFiles( ))
        .pipe(filterJS)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('assets/css'));
});

gulp.task('main-bower-files-sass', function(){
    var filterJS = gulpFilter('**/sass/**', { restore: true });
    return gulp.src('./_resources/bower/bower_components/')
        .pipe(filterJS)
        .pipe(filterJS.restore)
        .pipe(gulp.dest('_resources/sass/vendor'));
});



/**
 * Default task, running just `gulp` will compile the sass,
 * compile the jekyll site, launch BrowserSync & watch files.
 */
gulp.task('default', ['browser-sync', 'watch']);

