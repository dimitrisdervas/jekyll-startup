---
title: File Structure
---

### File Structure

- _includes
    - basic_components
    - html_components
    - js
    - css
- _resources
    - _uploaded
    - fonts
    - sass
    - bower/bower_components
- assets
    - images
    - css
    - js
- _config.yml
- _dev-config.yml
- _prod-config.yml
- node_modules
- gulpfile.js
- package.json