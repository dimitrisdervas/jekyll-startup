---
title: Maps
---

### Static Maps
[Staticmapmaker](http://staticmapmaker.com/)

### Dynamic Maps
- Google

        /_includes/js/maps/google-onemarker.js
        /_includes/js/maps/google-multiplemarker.js

- Google Styles

        - https://snazzymaps.com/
        - https://mapstyle.withgoogle.com/
        - http://www.mapstylr.com/
- Leaflet

        /_includes/js/maps/leaflet.js_