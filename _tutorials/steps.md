---
title: Steps
---
git init
add .gitignore
    node_modules

add package.json
    bower.json




create bower file
bower init

### SASS
main SASS file 
_resources/sass/styles.scss

bower sass files copy to 
_resources/vendor/

### JS

copy _data/pluginsSourceFiles/jsSourceFiles.yml


Run 
gulp main-bower-files-js

it will find js files from /_resources/bower/bower.json' to assets/css after concatenate
<script type="text/javascript" src="{{ site.baseurl }}/assets/js/vendor.js"></script>

### Images
It uses Sharp https://github.com/mahnunchik/gulp-responsive
[Sharp](http://sharp.dimens.io/en/stable/api/)
Run gulp images it will create all images to your config in  gulpfile.js
it will copy the same folder structure from the _resources/_uploaded/..._

