---
title: Bower files
---


    var gulp         = require('gulp');
    var uglify       = require('gulp-uglify');
    var concat       = require('gulp-concat');
    var mainBowerFiles = require('gulp-main-bower-files');
    var gulpFilter = require('gulp-filter');

JS

    gulp.task('main-bower-files-js', function(){
        var filterJS = gulpFilter('**/*.js', { restore: true });
        return gulp.src('./_resources/bower/bower.json')
            .pipe(mainBowerFiles( ))
            .pipe(filterJS)
            .pipe(concat('vendor.js'))
            .pipe(uglify())
            .pipe(gulp.dest('assets/js'));
    });


CSS

    gulp.task('main-bower-files-css', function(){
        var filterJS = gulpFilter('**/*.css', { restore: true });
        return gulp.src('./_resources/bower/bower.json')
            .pipe(mainBowerFiles( ))
            .pipe(filterJS)
            .pipe(concat('vendor.css'))
            .pipe(gulp.dest('assets/css'));
    });


SASS

    gulp.task('main-bower-files-sass', function(){
        var filterJS = gulpFilter('**/*.scss', { restore: true });
        return gulp.src('./_resources/bower/bower.json')
            .pipe(mainBowerFiles( ))
            .pipe(filterJS)
            .pipe(filterJS.restore) 
            .pipe(gulp.dest('_resources/sass/vendor'));
    });