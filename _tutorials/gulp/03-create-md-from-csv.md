---
title: create pages from csv
---

    var gulp         = require('gulp');
    var gracefulFs   = require('graceful-fs');
    var handlebars   = require('gulp-compile-handlebars');
    var rename       = require('gulp-rename');
    var baby         = require('babyparse');


    gulp.task('univadis', function() {
    var config = {
        collection: '_univadis/',
        csv       : '_data-temp/id-univadis-noduplicates',
        template  : '_gulp-templates/drugs/univadis.hbs'
        };
        gracefulFs.readFile('./'+config.csv+'.csv', 'utf8', function(err, data){
            if (err) throw err;
            parsed = baby.parse(data,{delimiter: ',',   newline: ''});
            rows = parsed.data;
            for(var i = 1; i < rows.length; i++) {
                var items = rows[i];
                var templateData = {
                    id: items[0],
                    drugnameinside: items[2],
                    drastiki: items[3],
                    farmakotexniki: items[4],
                    companyinside: items[5],
                    endeixeis: items[6],
                    sectiondose: items[7],
                    antendeixeis: items[8]
                };
                gulp.src(config.template)
                    .pipe(handlebars(templateData))
                    .pipe(rename(config.collection+'/'+items[2]+'.md'))
                    .pipe(gulp.dest('.'));
                }
          });
    });


Template to use to create the md files
univadis.hbs

    ---
    id: "{{id}}"
    title: "{{title}}"
    drugname: "{{drugnameinside}}"
    company: "{{company}}"
    drastiki: "{{drastiki}}"
    farmakotexniki: "{{farmakotexniki}}"
    company2: "{{companyinside}}"
    representative: "{{represantative}}"
    endeixeis: "{{endeixeis}}"
    dose: "{{sectiondose}}"
    antendeixeis: "{{antendeixeis}}"
    precautions: "{{precautions}}"
    interactions: "{{interactions}}"
    pregnancy: "{{pregnancy}}"
    driving: "{{driving}}"
    undiserableffects: "{{underisableeffects}}"
    overdose: "{{overdose}}"
    pharmacodynamic: "{{pharmadynamic}}"
    pharmacomovement: "{{pharmamovement}}"
    clinicsafety: "{{proclinicsafety}}"
    ekdoxa: "{{ekdoxa}}"
    incompatibles: "{{incompatibles}}"
    storage: "{{storageprecautions}}"
    usage: "{{usageinstructions}}"
    ---
