---
title: google webfonts
---



_resources/fonts/fonts.file

    # Tab-delimeted format
    Oswald  400,700 latin,latin-ext
    # Google format
    Roboto:500,500italic&subset=greek

gulpfile.js

    var gulp         = require('gulp');
    var googleWebFonts = require('gulp-google-webfonts');

    /**
     - Compile fonts
     */
    gulp.task('fonts', function () {
        var options = {
        fontsDir: 'googlefonts/',
        cssDir: 'googlecss/',
        cssFilename: 'myGoogleFonts.css'
        };
        return gulp.src('_resources/fonts/fonts.list')
        .pipe(googleWebFonts(options))
        .pipe(gulp.dest('_resources/fonts'))
        ;
    });
