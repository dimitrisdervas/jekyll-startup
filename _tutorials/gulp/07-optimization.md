---
title: OPTIMIZE
---

- css
    + /assets/css/styles.css
    + /assets/css/vendor.css
    + /_includes/css/critical.css

final css file ex. all-css.css must become after 
- concat styles.css + vendor.css
- minify
- uncss
- sort
- autoprefix
- px to em
 use uncss


        var critical = require('critical');
            /*
            * Critical CSS
            */
            gulp.task('critical', function () {
                critical.generate({
                    base: './',
                    src: '_site/index.html',
                    css: '_site/assets/css/styles.css',
                    dest: '_includes/css/critical.css',
                    width: 320,
                    height: 480,
                    minify: true
                });
            });


- images
    defer images - https://varvy.com/pagespeed/defer-images.html
    optimize images

- html
    + minify html

- js
    one file

    + https://varvy.com/pagespeed/defer-loading-javascript.html



<script type="text/javascript">
    function downloadJSAtOnload() {
    var element = document.createElement("script");
    element.src = "assets/js/components.js";
    document.body.appendChild(element);
    }
    if (window.addEventListener)
    window.addEventListener("load", downloadJSAtOnload, false);
    else if (window.attachEvent)
    window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;
</script>
