---
title: paths
---

Paths

        var config = {
            assetsDir: '_resources',
            bowerDir: 'vendor/bower_components',
            sassPattern: 'sass/**/*.scss',
            jsPattern: 'js/**/*.js',
            production: !!util.env.production,
            sourceMaps: !!util.env.production,
        };


STEPS
## File Structure

_resources
  sass
    styles.scss
    _layouts
    _styles
  js
  _uploaded


assets
 css
   styles.css


HEAD
css   - styles.css
fonts - 

FOOTER
js - 