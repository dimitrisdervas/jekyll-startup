---
title: sass compile and watch
---
    var gulp         = require('gulp');
    var browserSync  = require('browser-sync');
    var sass         = require('gulp-sass');
    var prefix       = require('gulp-autoprefixer');
    var cp           = require('child_process');
    var sourcemaps   = require('gulp-sourcemaps');
    var concat       = require('gulp-concat');
    var nano         = require('gulp-cssnano');
    var util         = require('gulp-util');
    var gulpif       = require('gulp-if');
    var plumber      = require('gulp-plumber');
    var uglify       = require('gulp-uglify');
    var imagemin     = require('gulp-imagemin');
    var pngquant     = require('imagemin-pngquant');
    var fs           = require('fs');
    var handlebars   = require('gulp-compile-handlebars');
    var rename       = require('gulp-rename');
    var baby         = require('babyparse');
    var mainBowerFiles = require('gulp-main-bower-files');
    var postcss      = require('gulp-postcss');
    var autoprefixer = require('gulp-autoprefixer');
    var sorting      = require('postcss-sorting');
    var assets       = require('postcss-assets');
    var responsive   = require('gulp-responsive');
    var shell        = require("gulp-shell");


VARIABLES:

    var config = {
        assetsDir: '_resources',
        bowerDir: 'vendor/bower_components',
        sassPattern: 'sass/**/*.scss',
        jsPattern: 'js/**/*.js',
        production: !!util.env.production,
        sourceMaps: !util.env.production,};


    /**
     - Compile files from _resources/sass into both [project-folder]/_site/css (for live injecting) and [project-folder]/css (for future jekyll builds)
     */
    gulp.task('styles', function () {
        app.addStyle([
            //config.bowerDir+'/font-awesome/css/font-awesome.css',
            config.assetsDir+'/sass/styles.scss'
        ], 'styles.css');
    });


    app.addStyle = function(paths, outputFilename){
        gulp.src(paths)
            .pipe(plumber())
            .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
            .pipe(sass({
                includePaths: ['scss'],
                onError: browserSync.notify
            }))
            .pipe(postcss([ require('autoprefixer') ],[assets({
          loadPaths: ['images/']
        })],[ sorting({ "empty-lines-between-children-rules": 1 , "sort-order": "zen" }) ]))
            .pipe(concat(outputFilename))
            .pipe(config.production ? nano() : util.noop())
            .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
            .pipe(gulp.dest('_site/assets/css'))
            .pipe(browserSync.reload({stream:true}))
            .pipe(gulp.dest('assets/css'));
    };.



WATCH ONLY SASS

    // WATCH ONLY SASS STYLES
    gulp.task('watch-only-styles', function () {
      gulp.watch(config.assetsDir+'/sass/*.scss', ['styles']);
    });
    gulp.task('watch-styles', ['browser-sync', 'watch-only-styles']);