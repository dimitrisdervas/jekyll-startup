---
title: create images for rensponsive webdesign
---

    var responsive     = require('gulp-responsive');  


    var config = {
    assetsDir: '_resources'
    }


    gulp.task('images', function () {
      // Select all images in assets/images and resize
      return gulp.src(config.assetsDir+'/_uploaded/**/*.{jpg,png}')
          .pipe(responsive({
            // Resize all JPG images to three different sizes: 200, 500, and 630 pixels
            '**/*.jpg': [{
              width: 200,
              rename: { suffix: '-200px' },
            }, {
              width: 500,
              rename: { suffix: '-500px' },
            }, {
              width: 630,
              rename: { suffix: '-630px' },
            }, {
              // Compress, strip metadata, and rename original image
              rename: { suffix: '-original' },
            }],
            // Resize all PNG images to be retina ready
            '**/*.png': [{
              width: 250,
            }, {
              width: 250 * 2,
              rename: { suffix: '@2x' },
            }],
          }, {
            errorOnUnusedImage: true,
            errorOnEnlargement: false,
            // Global configuration for all images
            // The output quality for JPEG, WebP and TIFF output formats
            quality: 70,
            // Use progressive (interlace) scan for JPEG and PNG output
            progressive: true,
            // Strip all metadata
            withMetadata: false,
          }))
        .pipe(gulp.dest('_site/assets/images/'))
        .pipe(gulp.dest('assets/images/'));
    });