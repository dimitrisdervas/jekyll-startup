---
title: browserify watch
---

VARIABLES:

    var config = {
        assetsDir: '_resources',
        bowerDir: 'vendor/bower_components',
        sassPattern: 'sass/**/*.scss',
        jsPattern: 'js/**/*.js',
        production: !!util.env.production,
        sourceMaps: !util.env.production,};

BUILD

    /**
     - Rebuild Jekyll & do page reload
     */
    gulp.task('jekyll-rebuild', ['jekyll-build'], function () {
        browserSync.reload();
    });

WATCH
CSS
JS
HTML PAGES
includes
COLLECTIONS
DATA FILES


    /**
     - Watch scss files for changes & recompile
     - Watch html/md files, run jekyll & reload BrowserSync
     - ADD ALL THE COLLECTIONS
     */
    gulp.task('watch', function () {
        gulp.watch(config.assetsDir+'/sass/**/*.scss', ['styles']);
        gulp.watch(config.assetsDir+'/js/**/*.js', ['scripts']);
        gulp.watch(['*.html',
                    '_config.yml',
                    '_layouts/**/*',
                    '_includes/**/*',
                    '_pages/**/*',
                    '_styleguide/**/*'],
                    ['jekyll-rebuild']);
    });

    /**
     - Wait for jekyll-build, then launch the Server
     */
    gulp.task('browser-sync', ['styles','fonts','jekyll-build'], function() {
        browserSync({
            server: {
                baseDir: '_site'
            }
        });
    });


    /**
     - Default task, running just `gulp` will compile the sass,
     - compile the jekyll site, launch BrowserSync & watch files.
     */
    gulp.task('default', ['browser-sync', 'watch']);

