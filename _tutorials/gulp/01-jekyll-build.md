---
title: jekyll build
---
    var gulp         = require('gulp');
    var shell        = require("gulp-shell");


        /**
         + Build the Jekyll Site
         + https://gist.github.com/azmenak/e699fc772e6dcd77e375
         */
        gulp.task("jekyll-build", shell.task([
          "cd " + './' + " && bundle exec jekyll build"
        ]));
