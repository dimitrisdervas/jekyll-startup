---
title: Coudcannon
---

Specific FRONT MATTER

- ### File Uploads

**Cloudcannon:**

        ---
          - _path:
          - _document-path:
          - _document:
          - _image_path:
          - _image:
          or
          - path:
          - document-path:
          - document:
          - image_path:
          - image:
        ---

        ---
        background_image_path: /images/background.png
        newsletter_document_path: /documents/2016/newsletter.pdf
        extra_styles_path: /styles/screen.css
        ---


**Siteleaf**


        ---
          - _asset:
          - _assets:
          - _file:
          - _files:
          - _image:
          - _images:
          or
          - asset:
          - assets:
          - file:
          - files:
          - image:
          - images:
        ---


- ### Single Select

**Cloudcannon:**
    - in config.yml

        ---
        styles:
          + Red
          + Blue
          + Green

        options:
          red: Red Shirt
          blue: Blue Sweater
          green: Green Jacket

        collections:
          + authors
        ---      


- FRONT MATTER - single number noun

          ---
          style: Green
          option: red
          author: george
          ---

**Siteleaf**
only collections

ex. collection people


      ---
      person: [single]
      or 
      people: [plural]
      ---

- ### Multiple Select
    - in config.yml

            styles:
              + Red
              + Blue
              + Green

            options:
              red: Red Shirt
              blue: Blue Sweater
              green: Green Jacket

            collections:
              + authors

    - FRONT MATTER - plural number noun
              ---
              styles:
                * Green
                * Blue
              options:
                * red
                * blue
              authors:
                * george
                * mike
              ---

The value saved to the each item in the front matter array depends on how the select is populated. Array items are saved as the value, keys are saved for objects and collection items are saved by filename.

If not populated from any source, categories and tags display a multiselect that supports adding options inline.


- ### Time: 

            ---
            _time
            ---
            ---
            opening_time: 8:00 am
            ---
- ### Date: 
            ---
            _date
            ---
            ---
            sale_start_date: 2015-01-09 00:00:00
            ---


**Siteleaf**


            ---
            - _datetime
            - _at
            - _date
            ---
            ---
            event_date:
            starts at:
            datetime:
            ---

- ### Datetime: 
            ---
            - datetime
            - at
            or 
            - _datetime
            - _at            
            ---
            ---
            date: 2015-07-15 12:00:00
            ---
- ### Color
            ---
            - _colour:
            - _color:
            - _rgb:
            - _hex:
            - _hsa:
            - _hsv:
            ---

            ---
            brand_colour: '#f05f40'
            ---
Quote hex colours, otherwise the hash symbol begins a YAML comment.


**Siteleaf**


      ---           
      color:
      colour:
      colors:
      colours:
      hex:
      rgb:
      rgba:
      hsl:
      hsla:
      hsv:
      hsva:
      ---



- ### number: 
            ---
            _number
            ---
            ---
            order: 12
            number: 3.14
            sort_number: 2
            ---
- ### boolean
          ---
          show_feature: true or false
          ---


**Siteleaf**


          ---
          show_feature: true or false
          or 
          - is_
          - has_
          - was_
          ---
          ---
          - is featured
          - has_registration
          - was-open
          ---

- ### Rich text:
        ---
          - _html
          - _markdown
          or
          - markdown:
          - html:
        ---

        ---
        markdown: |
          # Animals
          - Dogs
          - Cats

          > It's raining cats and dogs.


        sidebar_html: |
          <p>This paragraph has <em>emphasis</em> and <strong>strength</strong>.</p>
          <ol>
            <li>Walk</li>
            <li>Run</li>
          </ol>
          <p>Linking to <a href="/">index</a>.</p>
        ---


**Siteleaf**


        ---
          - _body
          - _description
          - _excerpt
          - _md
          - _text
        ---


Note: When you reference this field in your template, it won’t automatically output in HTML. You can achieve this though using the markdownify Liquid filter. For example:

        {{ page.caption_text | markdownify }}


- ### Long text: desciption

        ---
        description: Products are crafted in-house by hand, making each piece a labour of love and an exercise in craftsmanship.
        ---
- ###  Social 


        ---
        twitter: CloudCannonApp
        twitter_url: 'https://twitter.com/@jekyllrb'
        facebook: CloudCannon
        facebook_url: 'https://www.facebook.com/CloudCannon'
        google_plus: +CloudCannon
        default_google_plus: '117511497981903622103'
        google_plus_url: 'https://plus.google.com/+CloudCannon'
        github_username: CloudCannon
        github_url: 'https://github.com/jekyll'
        docs_github_url: 'https://github.com/CloudCannon/Documentation'
        instagram_url: 'https://www.instagram.com/purenewzealand/'
        email: support@cloudcannon.com
        email_address: support@cloudcannon.com
        ---


- ### Hidden from user:
        ---
         - _anyvariable
        ---
- ### Comment: 
    - in config.yml

          _comments:
            title: The page title
            output: Does this item have a dedicated page?
            brand_colour: The primary brand colour
            footer: Update the details in the footer


          defaults:
            + type: ''
              values:
                _comments:
                  title: The page title
                  output: Does this item have a dedicated page?
                  brand_colour: The primary brand colour
                  footer: Update the details in the footer
- ### defaults
    - in config.yml

              ---
              _defaults:
                - type: ''
                  values:
                    _defaults:
                      image_path: /images/placeholder.png
              ---

              ---
              _defaults:
                image_path: /images/placeholder.png
              images:
                - image_path: /images/sunset.png
                  title: Sunset

                # Adding an item to the array is prepopulated as:
                - image_path: /images/placeholder.png
                  title:
              ---
Array defaults also apply when editing CSV, YAML and JSON files.


DEFAULT FOR COLLECTIONS OR PAGES
_defaults.md
_defaults.html