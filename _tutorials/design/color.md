---
title: color
---


[Color Claim](http://www.vanschneider.com/colors/)
[Crayon](http://riccardoscalco.github.io/crayon/)
[Colourco](http://www.colourco.de/)
[Coolors](https://coolors.co/browser/latest/1)
[Material](https://material.google.com/style/color.html#color-color-palette)
(https://www.materialpalette.com/grey/purple)
A11y
(http://clrs.cc/a11y/)