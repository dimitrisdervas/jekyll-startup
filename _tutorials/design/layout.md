---
title: Layout
---


### Use Susy 2.2.12

[Documentation](http://susydocs.oddbird.net/en/latest/)

susy_config.scss


		$susy: (
		  flow: ltr,
		  math: fluid,
		  output: float,
		  gutter-position: after,
		  container: 1170px,
		  container-position: center,
		  columns: 4,
		  gutters: .25,
		  column-width: false,
		  global-box-sizing: content-box,
		  last-flow: to,
		);

		$<new-layout>: (
		  columns: 12,
		  container: 1170px,
		  gutters: .25,
		  output: float,
		/*   debug: (image: show), */
		);

		$susy-media: (
		  normal:  (
		    media: screen,
		    min-width: 993px,
		  ),
		  mobile:  (
		    media: screen,
		    max-width: 992px,
		  ),
		);

[Assymetrical Layout](https://zellwk.com/blog/asymmetric-layouts-with-susy/)
[demo](https://codepen.io/zellwk/pen/uvnyx)
		$susy: (
		  columns: 1 2 3 2 1,
		  output: isolate,
		  debug: (image: show)
		  );