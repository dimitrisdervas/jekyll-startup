---
title: Menus
---

### Navbar

### Sidebar-Slideout

### Bottom navigation

### Dropdown

### Slideout
[slideout](https://github.com/Mango/slideout)

[Priority](http://gijsroge.github.io/priority-nav.js/)

### INspiration

[Menu Inspiration](http://tympanus.net/Development/LineMenuStyles/)
(https://github.com/VPenkov/okayNav)
(https://github.com/lukejacksonn/GreedyNav)
(https://github.com/datchung/css-responsive-menu)
(https://github.com/robinpoort/vanilla-js-responsive-menu)