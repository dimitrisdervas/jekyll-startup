---
title: Tags in Jekyll
---

(DRY SOLUTION)[http://www.minddust.com/post/alternative-tags-and-categories-on-github-pages]

_layouts/post.html_

    {% assign category = site.tags | where: "slug", post.category %}
    {% assign category = category[0] %}
    {% if category %}
        {% capture category_content %}<a class="label" href="{{ category.url }}">{{ category.name }}</a>{% endcapture %}
    {% endif %}

    <p id="post-meta">{{ category_content }}</p>